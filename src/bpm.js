import $ from 'jquery'
import _ from 'lodash'

export async function getBPM () {
  return window.store.bpm
}

export async function setBPM (val) {
  $('.bpm-value').html(val)
  return getBPM()
}

export async function modifyBPM (val) {
  val = val || 10
  await setBPM(await getBPM() + val)
  return getBPM()
}

export async function getBeat () {
  return (60 * 1000) / (await getBPM())
}

export const increaseBPM = _.partial(modifyBPM, 10)
export const decreaseBPM = _.partial(modifyBPM, -10)
