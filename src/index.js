import 'babel-polyfill'
import $ from 'jquery'
import * as Key from 'tonal-key'

import { getBeat } from './bpm'
import { randomChord } from './music'

window.store = {
  // This will store all functions to be called on the beat
  //
  // They should have no arguments and store all important information in $store.
  onBeatQueue: [],

  // Id value of the beat interval
  onBeatInterval: null,

  currentBeat: null,
  // TODO make it so that changes to beatsInABar doesn't break the code.
  beatsInABar: 4,

  // Sets the global bpm and updates the UI.
  set bpm (x) {
    window.sessionStorage.setItem('jazzTrainerBpm', x)
    $('.bpm-value').html(parseInt(x))
    this.bpmChangeHooks.forEach(f => f())
  },
  get bpm () {
    return parseInt(window.sessionStorage.getItem('jazzTrainerBpm'))
  },
  // Changes to be made on bpm change should be pushed to this array.
  bpmChangeHooks: []
}

var $store = window.store

// Seal the store to disallow accidental adding/removing of keys
Object.seal($store)

// Let's animate. We want to add the 'inverted' class to the body.
async function invertBody () {
  let body = $('body')
  if (body.hasClass('inverted')) {
    body.removeClass('inverted')
  } else {
    body.addClass('inverted')
  }
}

/**
 * Append a random chord to the chord list.
 * @param {jQuery|HTMLElement} ul ul element containing the chords
 */
async function appendChord (ul) {
  let prevChords = $.makeArray($('#two-five-one-chords').find('.chord').map((i, v) => v.innerHTML)).reverse().splice(0, 2)
  let key = await randomChord({ excludeChords: prevChords })
  // let chords = Key.leadsheetSymbols(key + ' major')
  let chords = Key.leadsheetSymbols(
    ['Δ', '-7', '-7', 'Δ', '7', '-7', '°'],
    key + ' major')

  ul
    .append('<li><span class="chord">' + key + '</span></li>')
    .children(':last')
    .append($('<div/>', {
      'class': 'more-chords'
    }))
    .children('.more-chords')
    .append($('<div/>', {
      'class': 'sub-chord',
      text: chords[1]
    }))
    .append($('<div/>', {
      'class': 'sub-chord',
      text: chords[4]
    }))
    .append($('<div/>', {
      'class': 'sub-chord',
      text: chords[0]
    }))
}

async function populateChordList () {
  // delete everyting in the chords list
  let ul = $('#two-five-one-chords')
  ul.html('')
  for (let i = 0; i < 6; ++i) {
    await appendChord(ul)
  }
}

async function cycleChordList () {
  // Timeout to finish the animation at the first beat
  const animationDuration = 100
  const timeoutDuration = await getBeat() - animationDuration
  let ul = $('#two-five-one-chords')
  setTimeout(() => {
    $('#two-five-one-chords')
      .children('li:first')
      .slideUp(animationDuration, function () { $(this).remove() })
  },
  timeoutDuration
  )
  await appendChord(ul)
}

// This function is not async because it must immediately return an async
// function, not a promise.
function onBeatX (f, x) {
  return async function () {
    if ($store.currentBeat === x) {
      return f()
    }
  }
}

async function stepCounter () {
  let counter = $('.counter')
  let currentBeatInDOM = null
  let quarterBeat = (await getBeat()) / 4.0
  // Get all the children. Make the last invisible one visible. If there are not
  // invisible children, make all but the first invisible.
  if (counter.children('.invisible').length === 0 || counter.children('.invisible').length === $store.beatsInABar) {
    // If the first one is invisible, make it immediately visible
    if (counter.children(':first').hasClass('invisible')) {
      counter.children(':first')
        .removeClass('invisible')
    }
    currentBeatInDOM = counter.children(':first')

    // If everything is visible, make only the first visible
    counter.children(':not(:first)')
      .addClass('invisible')
    counter.find('span').addClass('invisible')
  } else {
    currentBeatInDOM = counter.children('.invisible:first')
    currentBeatInDOM
      .removeClass('invisible')
  }

  counter.find('.subdivision').addClass('invisible')
  currentBeatInDOM.find('.subdivision').each((ind, element) => {
    setTimeout(function () {
      $(element)
        .removeClass('invisible')
    }, quarterBeat * (ind + 1))
  })
}

/**
 * Steps the beat to the next
 */
async function stepBeat () {
  const nextBeat = $store.currentBeat + 1
  if (nextBeat <= $store.beatsInABar) {
    $store.currentBeat = nextBeat
  } else {
    $store.currentBeat = 1
  }
}

async function startTwoFiveOne () {
  $store.currentBeat = 1
  const beat = await getBeat()

  await populateChordList()

  // Reset the queue and give it new functions (just in case)
  $store.onBeatQueue.length = 0
  $store.onBeatQueue.push(stepBeat, stepCounter, onBeatX(cycleChordList, $store.beatsInABar))

  if ($store.onBeatInterval) {
    clearInterval($store.onBeatInterval)
  }
  $store.onBeatInterval = setInterval(() => $store.onBeatQueue.forEach(f => f()), beat)
}

$store.bpmChangeHooks.push(startTwoFiveOne)
function increaseBPM () {
  $store.bpm = $store.bpm + 10
}
function decreaseBPM () {
  $store.bpm = $store.bpm - 10
}

// TODO make the tempo automatically update
$('#invert-screen').click(invertBody)
$('.bpm-slower').click(decreaseBPM)
$('.bpm-faster').click(increaseBPM)
$('#start-two-five-one').click(startTwoFiveOne)

// NOTE: this file is getting out of hand
//
// TODO Split out this file into multiple modules as makes sense. Maybe a store
// module. Maybe look up how others implement stores in javascript.

function setupPage () {
  // Clear the counter (to reset state for parcel hot reloading)
  $('.counter').html('')

  // Filling the counter
  for (let i = 1; i <= $store.beatsInABar; ++i) {
    $('.counter').append(
      $('<li/>', {
        'class': `counter-${i} invisible`,
        text: i
      })
        .append($('<div/>', {
          'class': 'subdivision-container'
        })))
    $(`.counter-${i} div.subdivision-container`)
      .append($('<span/>', {
        'class': `counter-${i}-ee subdivision invisible`,
        text: '•'
      }))
      .append($('<span/>', {
        'class': `counter-${i}-an subdivision invisible`,
        text: '•'
      }))
      .append($('<span/>', {
        'class': `counter-${i}-aa subdivision invisible`,
        text: '•'
      }))
  }

  stepCounter()

  if (isNaN($store.bpm)) {
    $store.bpm = 150
  } else {
    // Else, activate the setter for bpm with the current bpm
    //
    // (const bpm exists just for standardjs not to freak out TODO figure out
    // how this *should* be done. Activating the setter like this feels wrong)
    const bpm = $store.bpm
    $store.bpm = bpm
  }
}

// Setup the page
setupPage()

console.log('JavaScript loaded!')

window.$ = $
